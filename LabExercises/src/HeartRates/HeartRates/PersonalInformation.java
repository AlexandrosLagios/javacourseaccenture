package HeartRates;

import javax.swing.*;
//import java.util.Scanner;

public class PersonalInformation
{
    private String firstName;
    private String lastName;
    private String dayOfBirth;
    private String monthOfBirth;
    private String yearOfBirth;
    //private JButton calculateHeartRateResultsButton;

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getDayOfBirth()
    {
        return dayOfBirth;
    }

    public String getMonthOfBirth()
    {
        return monthOfBirth;
    }

    public String getYearOfBirth()
    {
        return yearOfBirth;
    }

    public PersonalInformation()
    {
        String firstName = JOptionPane.showInputDialog("What is your first name?");
        this.firstName = firstName;
        System.out.println();
        String lastName = JOptionPane.showInputDialog("What is your last name?");
        this.lastName = lastName;
        System.out.println();
        String dayOfBirth = JOptionPane.showInputDialog("What was your day of birth?");
        this.dayOfBirth = dayOfBirth;
        System.out.println();
        String monthOfBirth = JOptionPane.showInputDialog("What was your month of birth?");
        this.monthOfBirth = monthOfBirth;
        System.out.println();
        String yearOfBirth = JOptionPane.showInputDialog("What was your year of birth?");
        this.yearOfBirth = yearOfBirth;
        System.out.println();
    }
}
