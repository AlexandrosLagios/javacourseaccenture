package HeartRates;

import javax.swing.*;
import java.util.Calendar;

public class HeartRates
{
    private String firstName;
    private String lastName;
    private String dayOfBirth;
    private String monthOfBirth;
    private String yearOfBirth;

    public HeartRates() {}

    public HeartRates(String firstName, String lastName, String dayOfBirth, String monthOfBirth, String yearOfBirth)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dayOfBirth = dayOfBirth;
        this.monthOfBirth = monthOfBirth;
        this.yearOfBirth = yearOfBirth;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getDayOfBirth()
    {
        return dayOfBirth;
    }

    public String getMonthOfBirth()
    {
        return monthOfBirth;
    }

    public String getYearOfBirth()
    {
        return yearOfBirth;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setDayOfBirth(String dayOfBirth)
    {
        this.dayOfBirth = dayOfBirth;
    }

    public void setMonthOfBirth(String monthOfBirth)
    {
        this.monthOfBirth = monthOfBirth;
    }

    public void setYearOfBirth(String yearOfBirth)
    {
        this.yearOfBirth = yearOfBirth;
    }

    public int yearsOfAge()
    {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - Integer.parseInt(yearOfBirth);
    }

    public int maxHeartRate()
    {
        return 220 - yearsOfAge();
    }

    public double targetUpperHeartRate()
    {
        return (double) maxHeartRate() * 0.85;
    }

    public double targetLowerHeartRate()
    {
        return (double) maxHeartRate() * 0.5;
    }

    public void heartRateResults ()
    {
        String message = "Your age is " + yearsOfAge() + " years, so your maximum suggested heart rate is "
                + maxHeartRate() + " beats per minute.\n" +
                "Your target heart rate range is between " + targetLowerHeartRate()
                + " and " + targetUpperHeartRate() + " beats per minute\n" +
                "Note: These formulas are estimates provided by the AHA. " +
                "Maximum and target heart rates may vary based on the health, fitness and gender of the individual.\n" +
                "Always consult a physician or a qualified health care professional before beginning or modifying an " +
                "exercise program.";
        JOptionPane.showMessageDialog(null, message);
    }
}