package HeartRates;

public class Main
{
    public static void main(String[] args)
    {
        PersonalInformation newPerson = new PersonalInformation();
        HeartRates HeartOfNewPerson = new HeartRates(newPerson.getFirstName(), newPerson.getLastName(),
                newPerson.getDayOfBirth(), newPerson.getMonthOfBirth(), newPerson.getYearOfBirth());
        HeartOfNewPerson.heartRateResults();
    }
}
