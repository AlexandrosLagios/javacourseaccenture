package Employees.Employees.Entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Company
{
    private String name;
    private String address;
    private double baseSalary;

    private HashMap<Integer, Employee> employees = new HashMap<>();
    private Set<String> types = new HashSet<String>();
    private int counter = 0;

    public Company() {}

    public Company(String name, String address, double baseSalary, HashSet<String> types)
    {
        this.name = name;
        this.address = address;
        this.baseSalary = baseSalary;
        this.types = types;
    }

    public int getCounter()
    {
        return counter;
    }

    public void setCounter(int counter)
    {
        this.counter = counter;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getBaseSalary()
    {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary)
    {
        this.baseSalary = baseSalary;
    }

    public Set<String> getTypes()
    {
        return types;
    }

    public void setTypes(Set<String> types)
    {
        this.types = types;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public HashMap<Integer, Employee> getEmployees()
    {
        return employees;
    }

    public void addEmployee(Employee employee)
    {
        counter++;
        employees.put(counter, employee);
        employee.setId(counter);
        calculateSalary(employee);
    }

    public void removeEmployee(int id)
    {
        Company unemployed = new Company("Unemployed", "Home", 0, new HashSet<>());
        Employee theEmployee = employees.get(id);
        employees.remove(id);
    }

    public Employee getEmployee(int id)
    {
        if (employees.get(id) == null)
        {
            System.out.println("There is no employee with this id in " + this.name);
            return null;
        }
        return employees.get(id);
    }

    public void calculateSalary(Employee employee)
    {
        double theSalary = getBaseSalary() + getBaseSalary() * 0.01 * employee.getBonus();
        employee.setSalary(theSalary);
    }

    public void promoteToManager (Employee employee, double bonus)
    {
        employee.setType("Manager");
        employee.setBonus(bonus);
        calculateSalary(employee);
    }
}
