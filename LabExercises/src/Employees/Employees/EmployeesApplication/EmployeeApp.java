package Employees.Employees.EmployeesApplication;

import Employees.Employees.Entities.Company;
import Employees.Employees.Entities.Employee;

import javax.swing.*;
import java.util.HashSet;
import java.util.Scanner;

public class EmployeeApp
{
    private static HashSet<String> types = new HashSet<>();
    private static int companyCounter = 0;


    public static Company createCompany()
    {
        String thCompanyName = JOptionPane.showInputDialog("What is the name of the company?");
        String theAddress = JOptionPane.showInputDialog("What is the address of the company?");
        String theBaseSalary = JOptionPane.showInputDialog("What is the base salary in the company?");
        HashSet<String> theTypes = inputTypes();

        Company newCompany = new Company(thCompanyName, theAddress, Double.parseDouble(theBaseSalary), theTypes);
        companyCounter++;
        return newCompany;
    }

    public static HashSet<String> inputTypes()
    {
        String numOfTypes = JOptionPane.showInputDialog("How many types of types of employee are there in the company?");

        return askForTypes(Integer.parseInt(numOfTypes));
    }

    public static HashSet<String> askForTypes(int numOfTypes)
    {
        if (numOfTypes > 0)
        {
            String type = JOptionPane.showInputDialog("Please enter a type of position: ");
            types.add(type);
            numOfTypes--;
            askForTypes(numOfTypes);
        }

        return types;
    }

    public static double getBonus()
    {
        String bonusString = JOptionPane.showInputDialog("Please enter the manager's bonus (2%-4%).");
        double theBonus = Double.parseDouble(bonusString);

        if (theBonus < 2 || theBonus > 4)
        {
            System.out.println("The bonus should be between 2 and 4%!");
            getBonus();
        }

        return theBonus;
    }

    public static void hireEmployee(Company company)
    {
        Scanner read = new Scanner(System.in);

        String theName = JOptionPane.showInputDialog("What is the employee's name?");

        String theType = JOptionPane.showInputDialog("What is the employee's type?");


        Employee employee = new Employee(theName, theType);

        if (read.next().equals("Manager"))
        {
            employee.setBonus(getBonus());
            company.addEmployee(employee);
        }
        else
        {
            employee.setBonus(0);
        }

        company.addEmployee(employee);
    }

    public static void hireEmployee(Company company, String employeeName, String employeeType)
    {
        Employee employee = new Employee(employeeName, employeeType);

        if (employeeType.equals("Manager"))
        {
            Scanner read = new Scanner(System.in);
            employee.setBonus(getBonus());;
        }
        else
        {
            employee.setBonus(0);
        }

        company.addEmployee(employee);
    }

    public static void hireEmployee(Company company, Employee employee)
    {
        company.addEmployee(employee);
    }

    public static void fireEmployee(Company company, Employee employee)
    {
        company.removeEmployee(employee.getId());
    }

    public static void fireEmployee(Company company, int id)
    {
        company.removeEmployee(id);
    }

    public static void promote(Company company, Employee employee)
    {
        Scanner read = new Scanner(System.in);
        company.promoteToManager(employee, getBonus());
    }

    public static void main(String[] args)
    {
        Company company;
        HashSet<String> jobTypes = new HashSet<String>();
        jobTypes.add("Developer");
        jobTypes.add("Manager");
        company = new Company("Twitter", "Kifisias 3", 700, jobTypes);
        Employee employee = new Employee("Mpampis", "Developer");
        hireEmployee(company, employee);
        System.out.println(company.getEmployee(1).getSalary());
        promote(company, employee);
        System.out.println(employee.getSalary());
        fireEmployee(company, employee);
        company.getEmployee(1);
    }
}
