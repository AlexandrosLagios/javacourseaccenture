package Generics;

import java.util.Comparator;

public class Employee implements Comparable<Employee>
{
	private int id;
	private String name;
	private String address;
	private static int counter;
	private double salary;

	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Employee(  String name, String address) {
		super();
		counter++;
		id=counter;
		this.name = name;
		this.address = address;
	}
	public Employee(String name) {
		super();
		counter++;
		id=counter;
		this.name = name;
	}
	public Employee() {
		super();
		counter++;
		id=counter;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", address=" + address +
				", salary=" + salary + "]";
	}

	public double getSalary()
	{
		return salary;
	}

	public void setSalary(double salary)
	{
		this.salary = salary;
	}

	@Override
	public int compareTo(Employee other)
	{
		return (int) (this.salary - other.getSalary());
	}
}
