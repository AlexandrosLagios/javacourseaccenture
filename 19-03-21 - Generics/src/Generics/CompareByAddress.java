package Generics;

import java.util.Comparator;

public class CompareByAddress<T extends Employee> implements Comparator<T>
{
    @Override
    public int compare(T o1, T o2)
    {
        return (int) (o1.getAddress().length() - o2.getAddress().length());
    }

    @Override
    public boolean equals(Object obj)
    {
        return false;
    }
}
