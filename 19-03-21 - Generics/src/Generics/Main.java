package Generics;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Main
{
    public static <T extends Comparable <T>> T minimumSalary(T[] elements)
        {
            T lowest = elements[0];
            for (int i = 1; i < elements.length; i++)
            {
                if(lowest.compareTo(elements[i]) > 0)
                {
                    lowest = elements[i];
                }
            }

        return lowest;
    }

    public static <T extends Comparable <T>> T minimumSalary(List<T> elements)
    {
        T lowest = elements.get(0);
        for (T element: elements)
        {
            if(lowest.compareTo(element) > 0)
            {
                lowest = element;
            }
        }

        return lowest;
    }

    public static <T, K extends Comparator<T>> T minimumLength(List<T> elements, K comparator)
    {
        T shortest = elements.get(0);
        for (T element: elements)
        {
            if(comparator.compare(element, shortest) < 0)
            {
                shortest = element;
            }
        }

        return shortest;
    }

    public static void main(String[] args)
    {
        Employee[] employees = new Employee[4];
        Employee mitsos = new Employee("mafdfgfgdfgafdg", "afjgoadfhpoaidfh");
        mitsos.setSalary(790);
        employees[0] = mitsos;
        Employee giorgos = new Employee("Giorgofdags", "afd");
        giorgos.setSalary(400.50);
        employees[1] = giorgos;
        Employee kostas = new Employee("Kostas", "afgadfgad");
        kostas.setSalary(670);
        employees[2] = kostas;
        Employee vaggelis = new Employee("fafgadfgadfgadfg", "aagadfhaghfs");
        vaggelis.setSalary(1045);
        employees[3] = vaggelis;
        List<Employee> employeeList = Arrays.asList(employees);
//        System.out.println(minimumSalary(employees).toString());
//        System.out.println(minimumSalary(employeeList).getName());
        CompareByName compareByName = new CompareByName();
        System.out.println("The employee with the shortest name is: "+
                minimumLength(employeeList, compareByName).getName());
        CompareByAddress compareByAddress = new CompareByAddress();
        System.out.println("The employee with the shortest address is: " +
                minimumLength(employeeList, compareByAddress).getName());
    }
}
