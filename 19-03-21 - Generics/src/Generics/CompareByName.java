package Generics;

import java.util.Comparator;

public class CompareByName<T extends Employee> implements Comparator<T>
{
    @Override
    public int compare(T o1, T o2)
    {
        return (int) (o1.getName().length() - o2.getName().length());
    }

    @Override
    public boolean equals(Object obj)
    {
        return false;
    }
}
