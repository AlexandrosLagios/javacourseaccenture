package Employees2;

public class Employee
{
    private Company employer;
    private int id;
    private String name;
    private double salary;
    private double bonus;
    private String type;

    public Employee(){};
    public Employee(String name, String type)
    {
        this.name = name;
        this.type = type;
    }

    public Company getEmployer()
    {
        return employer;
    }

    public void setEmployer(Company employer)
    {
        this.employer = employer;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getSalary()
    {
        return salary;
    }

    public void setSalary()
    {
        this.salary = employer.getBaseSalary() + employer.getBaseSalary() * 0.01 * bonus;
    }

    public void setSalary(double salary)
    {
        this.salary = salary;
    }

    public double getBonus()
    {
        return bonus;
    }

    public void setBonus(double bonus)
    {
        this.bonus = bonus;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
