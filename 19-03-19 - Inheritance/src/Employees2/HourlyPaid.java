package Employees2;

public class HourlyPaid extends Employee
{
    private int hoursPerWeek;
    private double salaryPerHour;
    public HourlyPaid()
    {
        super();
    }
    public HourlyPaid(String name, String address, int hoursPerWeek, double salaryPerHour)
    {
        super(name, address);
        this.hoursPerWeek=hoursPerWeek;
        this.salaryPerHour = salaryPerHour;
    }

    @Override
    public String toString()
    {
        return super.toString() +"HourlyPaid{" +
                "hoursPerWeek=" + hoursPerWeek +
                ", salaryPerHour=" + salaryPerHour +
                "} ";
    }
}
