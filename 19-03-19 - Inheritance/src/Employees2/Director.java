package Employees2;

public interface Director
{
    void giveOrder(Employee employee, String command);
}
