package Employees2;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class Company
{
    private String name;
    private String address;
    private double baseSalary;
    private HashMap<Integer, Employee> employees = new HashMap<>();
    private Set<String> types = new HashSet<String>();
    private int counter = 0;


    public Company()
    {

    }

    public Company(String name, String address, double baseSalary, HashSet<String> types)
    {
        this.name = name;
        this.address = address;
        this.baseSalary = baseSalary;
        this.types = types;
    }

    public int getCounter()
    {
        return counter;
    }

    public void setCounter(int counter)
    {
        this.counter = counter;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getBaseSalary()
    {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary)
    {
        this.baseSalary = baseSalary;
    }

    public Set<String> getTypes()
    {
        return types;
    }

    public void setTypes(Set<String> types)
    {
        this.types = types;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void addEmployee(Employee employee)
    {
        counter++;
        employee.setEmployer(this);
        employees.put(counter, employee);
        employee.setId(counter);
    }

    public void removeEmployee(int id)
    {
        Company unemployed = new Company("Unemployed", "Home", 0, new HashSet<>());
        employees.get(id).setEmployer(unemployed);
    }

    public Employee getEmployee(int id)
    {
        return employees.get(id);
    }

    /**
     * Displays in the standard output the number of HoulyPaid employees,
     * how many of them are paid more than 20€ per hour,
     * the number of MonthlyPaid employees
     * how many of them are paid above 1000€ per hour.
     */
    public void simpleStatistics(int a, double b)
    {
        Iterator it = employees.entrySet().iterator();
        int numOfHourly = 0;
        int numOfHourlyMoreThan20 = 0;
        int numOfMonthly = 0;
        int numOfMonthlyMoreThan1000 = 0;
        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry)it.next();
            if (pair.getValue() instanceof HourlyPaid)
            {
                numOfHourly++;
                if (((HourlyPaid) pair.getValue()).getSalary() > 20)
                {
                    numOfHourlyMoreThan20
                }
            }
            else if (pair.getValue() instanceof MonthlyPaid)
            {
                numOfMonthly++;
                if (((MonthlyPaid) pair.getValue()).getSalary() > 20)
                {
                    numOfHourlyMoreThan20
                }
            }

            PrintWriter pw;
            {
                try
                {
                    pw = new PrintWriter("C:\\employeeStats.txt");
                    pw.println();
                } catch (FileNotFoundException e)
                {
                    System.out.println("Error occured.");
                }
            }
        }
    }
}
