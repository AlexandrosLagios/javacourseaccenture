package Employees;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Employee implements Serializable
{
    private int id;
    private String name;
    private String address;
    private static int counter;

    public Employee()
    {
        counter++;
    }

    public Employee(int id, String name, String address)
    {
        counter++;
        this.id = counter;
        this.name = name;
        this.address = address;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    @XmlElement
    public void setName(String name)
    {
        this.name = name;
    }

    public String getAddress()
    {
        return address;
    }

    @XmlElement
    public void setAddress(String addresss)
    {
        this.address = address;
    }

    @Override
    public String toString()
    {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
