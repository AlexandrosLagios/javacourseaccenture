package Employees;

import java.io.FileOutputStream;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;


public class Main {

    public static void main(String[] args)
    {
        System.out.println("Creating class example.");
        System.out.println("Creating Employee object, using default constructor.");

        Employee employee = new Employee(1, "Mitsaras", "Kapou 27");

        Employee employee2 = new Employee();
        employee2.setName("Kostas");
        employee2.setAddress("Gravias 53");

        System.out.println("Employee created: " + employee.toString());

        try
        {
            File file = new File("C:\\Users\\HP\\Alexandros\\Projects\\javacourseaccenture\\Employees\\"
                    + employee2.getName() + ".xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Employee.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(employee2, file);
            jaxbMarshaller.marshal(employee2, System.out);
        }
        catch (JAXBException e)
        {
            e.printStackTrace();
        }
    }
}
