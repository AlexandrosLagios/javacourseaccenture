package Employees;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JSONFileWrite {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws IOException {

        Employee employee = new Employee(1, "Mitsaras", "Kapou 27");

        Employee employee2 = new Employee();
        employee2.setName("Kostas");
        employee2.setAddress("Gravias 53");

        JSONObject obj = new JSONObject();
        obj.put("Name", "Employees");
        obj.put("Author", "Alexandros Lagios");

        JSONArray employees = new JSONArray();
        employees.add("Employee " + employee.getId() + ": " + employee.toString());
        employees.add("Employee: " + employee.getId() + ": " + employee2.toString());
        obj.put("Employee List", employees);


        try (FileWriter file = new FileWriter("C:\\Users\\HP\\Alexandros\\Projects\\javacourseaccenture\\Employees\\" +
                "Employees.txt"))
        {
            file.write(obj.toJSONString());
            System.out.println("Successfully Copied JSON Object to File...");
            System.out.println("\nJSON Object: " + obj);
        }
    }
}
