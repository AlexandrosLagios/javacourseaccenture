package Employees;

import java.util.HashSet;
import java.util.Set;

public class Company
{
    private static final int NUMOFDIRECTORS = 5;
    private String name;
    private String address;
    private double baseSalary;
    private Employee[] directors;
    private Set<Employee> employees = new HashSet<>();
    private Set<String> types = new HashSet<>();

    public Company() {}

    public Company(String name, String address, double baseSalary, HashSet<String> types)
    {
        this.name = name;
        this.address = address;
        this.baseSalary = baseSalary;
        this.types = types;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getBaseSalary()
    {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary)
    {
        this.baseSalary = baseSalary;
    }

    public Set<String> getTypes()
    {
        return types;
    }

    public void setTypes(Set<String> types)
    {
        this.types = types;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
