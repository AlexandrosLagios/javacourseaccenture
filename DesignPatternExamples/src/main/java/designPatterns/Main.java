package designPatterns;

import designPatterns.templateMethod_iterator.Product;
import designPatterns.templateMethod_iterator.ProductCreator;
import designPatterns.templateMethod_iterator.ProductIterator;
import designPatterns.templateMethod_iterator.TypeOfProduct;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ProductCreator productCreator = new ProductCreator();
        List<Product> products = new ArrayList<Product>();
        Product product = productCreator.createProduct(TypeOfProduct.KNIFE);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.PEN);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.PEN);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.KNIFE);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.PEN);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.SPOON);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.TOOTHBRUSH);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.KNIFE);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.TOOTHBRUSH);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.TOOTHBRUSH);
        products.add(product);
        product = productCreator.createProduct(TypeOfProduct.TOOTHBRUSH);
        products.add(product);
        ProductIterator iterator = new ProductIterator(products);
        System.out.println(iterator.countProducts());
    }
}
