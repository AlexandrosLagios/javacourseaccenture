package designPatterns.templateMethod_iterator;

import lombok.Data;

import java.util.Date;

@Data
public class Pen extends Product {
    private Date fromDate;
    private Date toDate;

    public Pen() {
        System.out.println("This is a pen!");
    }
}
