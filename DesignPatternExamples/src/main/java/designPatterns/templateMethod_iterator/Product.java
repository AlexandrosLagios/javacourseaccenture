package designPatterns.templateMethod_iterator;

import lombok.Data;

@Data
public abstract class Product {
    private int id;
    private String productName;
    private double price;
    private String type;

    public void printProductType() {
        System.out.println(this.type);
    }
}
