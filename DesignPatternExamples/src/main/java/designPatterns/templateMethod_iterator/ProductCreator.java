package designPatterns.templateMethod_iterator;

public class ProductCreator {

    public Product createProduct(TypeOfProduct type) {
        Product product = null;
        switch (type) {
            case KNIFE:
                product = new Knife();
                product.setType("Knife");
                break;
            case PEN:
                product = new Pen();
                product.setType("Pen");
                break;
            case SPOON:
                product = new Spoon();
                product.setType("Spoon");
                break;
            case TOOTHBRUSH:
                product = new Toothbrush();
                product.setType("Toothbrush");
                break;
        }
        return product;
    }
}
