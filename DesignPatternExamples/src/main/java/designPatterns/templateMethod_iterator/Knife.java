package designPatterns.templateMethod_iterator;

public class Knife extends Product {
    public Knife() {
        System.out.println("This is a knife!");
    }
}
