package designPatterns.templateMethod_iterator;

import java.util.*;

public class ProductIterator {
    List<Product> products = new ArrayList<Product>();
    public ProductIterator(List<Product> products) {
        this.products = products;
    }

    public String countProducts() {
        StringBuilder result = new StringBuilder();
        result.append("The counts of the different types of product are the following: \n");
        Map<String, Integer> types = new HashMap<String, Integer>();
        for(TypeOfProduct typeOfProduct: TypeOfProduct.values()) {
            String type = typeOfProduct.toString().toLowerCase();
            types.put(type, 0);
        }

        for(Product product: products) {
            String productType = product.getType().toLowerCase();
            int currentCount = types.get(productType);
            types.put(product.getType().toLowerCase(), currentCount+1);
        }

        for (Map.Entry<String, Integer> type : types.entrySet()) {
            result.append("There are " + type.getValue() + " products of type " + type.getKey() + ".\n");
        }

        return result.toString();
    }
}
