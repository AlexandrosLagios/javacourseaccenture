package designPatterns.templateMethod_iterator;

import lombok.Data;

import java.util.Date;

@Data
public class Toothbrush extends Product {
    private Date dateOfDelivery;

    public Toothbrush() {
        System.out.println("This is a toothbrush.");
    }
}
