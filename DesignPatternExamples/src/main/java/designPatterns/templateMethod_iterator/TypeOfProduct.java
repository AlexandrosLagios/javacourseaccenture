package designPatterns.templateMethod_iterator;

public enum TypeOfProduct {
    KNIFE,
    PEN,
    SPOON,
    TOOTHBRUSH
}
