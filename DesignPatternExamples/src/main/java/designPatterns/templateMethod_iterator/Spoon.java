package designPatterns.templateMethod_iterator;

public class Spoon  extends Product {
    public Spoon() {
        System.out.println("This is a spoon");
    }
}
