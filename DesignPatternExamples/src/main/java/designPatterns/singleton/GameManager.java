package designPatterns.singleton;

import lombok.Getter;

    public class GameManager {
    @Getter private int numberOfPlayers = 1;
    private GameManager() {}
    private static GameManager gm_instance = null;

    public static GameManager getInstance() {
        if(gm_instance == null) {
            gm_instance = new GameManager();
        }
        return gm_instance;
    }
}
