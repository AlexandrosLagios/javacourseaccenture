package designPatterns.singleton;

public class Main {
    public static void main(String[] args) {
        GameManager gm = GameManager.getInstance();
        int numberOfPlayers = gm.getNumberOfPlayers();
        System.out.println(numberOfPlayers);
    }
}
