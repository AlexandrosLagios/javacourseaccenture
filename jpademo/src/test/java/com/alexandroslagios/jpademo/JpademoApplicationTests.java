package com.alexandroslagios.jpademo;

import com.alexandroslagios.jpademo.model.Book;
import com.alexandroslagios.jpademo.repository.BookRepository;
import com.alexandroslagios.jpademo.services.BookService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@//SpringBootTest
@DataJpaTest
@Slf4j
public class JpademoApplicationTests {

	@Autowired
	private BookService bookService;
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testingFindByTitle() {
		Book donQuixote = new Book();
		donQuixote.setNumberOfPages(560);
		donQuixote.setTitle("Don Quixote");
		donQuixote.setAuthor("Miguel De Cervantes");
		bookService.saveBook(donQuixote);
		
		log.info("TESTING FIND BY TITLE BOOK");
	}

}
