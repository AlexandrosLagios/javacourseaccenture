package com.alexandroslagios.jpademo.repository;

import com.alexandroslagios.jpademo.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
