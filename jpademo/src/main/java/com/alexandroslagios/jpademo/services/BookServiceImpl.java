package com.alexandroslagios.jpademo.services;

import com.alexandroslagios.jpademo.model.Book;
import com.alexandroslagios.jpademo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookRepository bookRepository;
    
    @Override
    public void saveBook(Book book) {
        bookRepository.save(book);
    }
}
