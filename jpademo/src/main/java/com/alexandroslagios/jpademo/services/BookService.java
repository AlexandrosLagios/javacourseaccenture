package com.alexandroslagios.jpademo.services;

import com.alexandroslagios.jpademo.model.Book;

public interface BookService {
    public void saveBook(Book book);
}
