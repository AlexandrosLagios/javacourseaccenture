package com.alexandroslagios.jpademo.services;

import com.alexandroslagios.jpademo.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

@Service
public class DBInit implements ApplicationListener<ApplicationReadyEvent> {
    
    @Autowired
    BookService bookService;
    
    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        Book donQuixote = new Book();
        donQuixote.setNumberOfPages(560);
        donQuixote.setTitle("Don Quixote");
        donQuixote.setAuthor("Miguel De Cervantes");
        bookService.saveBook(donQuixote);
    }
}
