package Lessons;

import java.util.Scanner;

public class HelloWorld
{

    public static void forLoop ()
    {
        for (int i =1; i <= 10; i++)
        {
            System.out.println(i);
        }
    }

    public static void whileLoop ()
    {
        int a = 1;
        int product;
        while (a <= 10)
        {
            product = 2 * a;
            System.out.println(product);
            a++;
        }
    }

    public static String calculateGrade ()
    {
        char grade;
        Scanner readCharacter = new Scanner(System.in);
        System.out.println("Enter a grade: ");
        grade = readCharacter.next().charAt(0);

        String message;

        switch (grade)
        {
            case 'A':
                message = "Very Good";
                break;
            case 'B':
                message = "Try More";
                break;
            case 'C':
                message = "Oops";
                break;
            default:
                message = "No grade?";
                break;
        }
        return message;
    }

    public static String upperCase (String input)
    {
        String s1 = input;
        return s1.toUpperCase();
    }

    public static String first10Characters (String input)
    {
        if (input.length() >= 10)
        {
            return input.substring(0, 10);
        }

        return input;
    }

    public static int lengthOfWord (String input)
    {
        String s1 = input;
        return s1.length();
    }

    public static String checkForPassword (String input)
    {
        System.out.println("Is this a password? (yes/no)");
        Scanner read = new Scanner(System.in);
        String message;
        String answer = read.next();

        if (answer.equals("yes"))
        {
            message = "Yes, this is a password.";
        }
        else if (answer.equals("no"))
        {
            message = "No, this is not a password.";
        }
        else
        {
            message = "Please write yes or no";
            checkForPassword(input);
        }

        return message;
    }

    public static void reverseWord (String input)
    {
        for (int i = input.length() - 1; i >= 0; i--)
        {
            System.out.print(input.charAt(i));
        }
    }

    public static void main(String[] args)
    {
//        System.out.println("Hello World!");
//        System.out.println("I'm Alexandros and I'm training in Java.");
//        System.out.println("I don't want to write Hello World again.");

//        System.out.println(calculateGrade());
//        forLoop();
//        whileLoop();
        Scanner readWord = new Scanner(System.in);
        System.out.println("Write something: ");
        String s = readWord.next();
        System.out.println(first10Characters(s));
        System.out.println(checkForPassword(s));
        reverseWord(s);

    }
}
