package com.alexandroslagios;

import com.alexandroslagios.pojos.Item;
import com.alexandroslagios.services.StaxParser;
import com.alexandroslagios.services.StaxWriter;

import java.util.List;

public class Main {
  public static void main(String[] args) throws Exception {
    List<Item> list = StaxParser.readConfig("services.xml");
//
//    Item item = new Item();
//    System.out.println(list);
    list.add(new Item("3/5/2011", "3", "4", "5", "6"));
    list.add(new Item("3/5/45", "6463", "ghwgr4", "thw5", "6wth"));
    
//    StaxWriter.saveConfig(list, "services2.xml");
    System.out.println(list);
//    StaxWriter.saveToJSON(list, "servicesJSon");
    StaxWriter.saveToJSONWithJackson(list);
  }
}
