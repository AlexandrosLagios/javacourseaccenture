package model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Employee implements Serializable
{
    private int id;
    private String name;
    private String address;
    private double salary;

    public Employee()
    {
    }

    public Employee(String name, String address)
    {
        this.name = name;
        this.address = address;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    @XmlElement
    public void setName(String name)
    {
        this.name = name;
    }

    public String getAddress()
    {
        return address;
    }

    @XmlElement
    public void setAddress(String addresss)
    {
        this.address = address;
    }

    public double getSalary(double salary)
    {
        return this.salary;
    }

    public void setSalary(double salary)
    {
        this.salary = salary;
    }
    @Override
    public String toString()
    {
        return id + ", " + name + ", " + address;
    }
}
