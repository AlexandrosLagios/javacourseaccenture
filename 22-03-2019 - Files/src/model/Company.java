package model;

import java.util.*;

public class Company
{
    private static int counter = 0;
    private String name;
    private String address;
    private double baseSalary;
    private Map<Integer, Employee> employees = new HashMap<>();
    //private List<Employee> employees = new ArrayList<>();
    private Set<String> types = new HashSet<>();

    public Company() {}

    public Company(String name, String address, double baseSalary)
    {
        this.name = name;
        this.address = address;
        this.baseSalary = baseSalary;
        this.types = types;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public double getBaseSalary()
    {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary)
    {
        this.baseSalary = baseSalary;
    }

    public Set<String> getTypes()
    {
        return types;
    }

    public void setTypes(Set<String> types)
    {
        this.types = types;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void addEmployee(Employee employee)
    {
        counter++;
        employees.put(counter, employee);
        employee.setId(counter);
        employee.setSalary(this.baseSalary);
    }

    public Map<Integer, Employee> getEmployees()
    {
        return employees;
    }

//    public List<Employee> getEmployees()
//    {
//        return this.employees;
//    }
}
