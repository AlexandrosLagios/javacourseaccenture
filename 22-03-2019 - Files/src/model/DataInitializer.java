package model;

import java.util.Map;

public class DataInitializer
{
    private Company company;
    public Map<Integer, Employee> initializeData()
    {
        Company company = new Company("Dropbox", "24, Musk Lane", 1000);
        Employee mitsos = new Employee("Mitsos","Naxou 22");
        Employee giorgos = new Employee("Giorgos", "Tinou 65");
        Employee orestis = new Employee("Orestis", "Parou 34");
        Employee iason = new Employee("Iason", "Rodou 21");
        company.addEmployee(mitsos);
        company.addEmployee(giorgos);
        company.addEmployee(orestis);
        company.addEmployee(iason);
        //System.out.println(company.getEmployees().get(0).toString());

        Map<Integer, Employee> employees = company.getEmployees();
        //List<Employee> employees = company.getEmployees();
        return employees;
    }

    public Company getCompany()
    {
        return company;
    }

    public void setCompany(Company company)
    {
        this.company = company;
    }
}
