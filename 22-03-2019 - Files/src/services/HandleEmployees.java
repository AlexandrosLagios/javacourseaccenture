package services;

import model.Company;
import model.Employee;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HandleEmployees
{

    private Company company;
    private final String filepath;
    //private final String fileName;

    public HandleEmployees(Company company,String filepath)
    {
        this.filepath = filepath;
        this.company = company;
    }

    public void writeEmployeeMapToFile()
    {
        try
        {
            for (Map.Entry<Integer, Employee> employee : this.company.getEmployees().entrySet())
            {
                writeEmployeeToFile(employee.getValue());
            }
        }
        catch (NullPointerException npe)
        {
            System.out.println(npe.toString());
        }
    }
    public void writeEmployeeToFile(Employee employee)
    {

        try
        {
            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(employee);
            objectOut.close();
            //System.out.println("Employee " + employee.getName() + " was succesfully written to the file");

        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public List<Employee> getEmployeesFromFile(File file)
    {
        FileInputStream employees = null;
        try
        {
            employees = new FileInputStream(file);
        }
        catch (FileNotFoundException e)
        {
            //e.printStackTrace();
        }
        ObjectInputStream ois = null;
        try
        {
             ois = new ObjectInputStream(employees);
        }
        catch (IOException e)
        {
            //e.printStackTrace();
        }

        List<Employee> newEmployeeList = new ArrayList<>();

        for (int i = 0; i < company.getEmployees().size(); i++)
        {
            try
            {
                newEmployeeList.add((Employee)ois.readObject());
            }
            catch (IOException e)
            {
                //e.printStackTrace();
            }
            catch (ClassNotFoundException e)
            {
                //e.printStackTrace();
            }
        }

        return newEmployeeList;
    }



    public Company getCompany()
    {
        return this.company;
    }
}
