package services;

import model.Employee;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.*;

public class FileService
{
    public void writeEmployeesToFile(Map<Integer, Employee> employees, String fileName) throws FileNotFoundException
    {
        PrintWriter pw = new PrintWriter(fileName);
        for (Map.Entry<Integer, Employee> employeeEntry : employees.entrySet())
        {
            pw.println(employeeEntry.getValue().toString());
        }
        pw.close();
    }

    public List<Employee> readEmployeesFromFile(String fileName) throws IOException
    {
        List<Employee> employees = new ArrayList<>();
        FileReader fr = new FileReader(fileName);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        while (line != null)
        {
            line.trim();
            String[] words = line.split(", ");
            Employee employee = new Employee(words[1], words[2]);
            employee.setId(Integer.parseInt(words[0]));
            employees.add(employee);
            line = reader.readLine();
        }
        reader.close();
        fr.close();
        return employees;
    }
}
