import model.Company;
import model.DataInitializer;
import model.Employee;
import services.FileService;
import services.HandleEmployees;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;


public class Main
{
    public static void main(String args[])
    {
        DataInitializer di = new DataInitializer();
        Map<Integer, Employee> employees = di.initializeData();
        String fileName =
                "C:\\Users\\HP\\Alexandros\\Projects\\javacourseaccenture\\Employees\\Employees.txt";

        FileService fs = new FileService();
        try
        {
            fs.writeEmployeesToFile(employees, fileName);
        }
        catch (FileNotFoundException e)
        {
            System.out.println(e.toString());
        }
        try
        {
            List<Employee> employeeList = fs.readEmployeesFromFile(fileName);
            System.out.println(employeeList.get(0).toString());
            System.out.println(employeeList.get(1).toString());
            System.out.println(employeeList.get(2).toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
