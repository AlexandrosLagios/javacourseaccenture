package com.alexadnroslagios.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HelloJavaDb {
    Connection conn;

    public static void main(String[] args) throws SQLException {
        HelloJavaDb app = new HelloJavaDb();

        app.connectionToMSSQLServer();
        app.normalDbUsage();
//        UserService userService = new UserService();
//        userService.addColumn("position");
//        userService.setUserName("Mitsos");
    }

    public void connectionToMSSQLServer() throws SQLException {


        // -------------------------------------------
        // URL format is
        // jdbc:derby:<local directory to save data>
        // -------------------------------------------
        //String dbUrl = "jdbc:derby:C:\\Users\\HP\\Alexandros\\Projects\\SQL\\JDBCTest;create=true";
        String dbUrl = "jdbc:sqlserver://localhost:1433;instance=SQLEXPRESS;databaseName=Test;";
        String user="alexandroslagios";
        String password="1234";
        conn = DriverManager.getConnection(dbUrl, user, password);
    }

    public void normalDbUsage()  throws SQLException{
        Statement stmt = conn.createStatement();

        // drop table
        // stmt.executeUpdate("Drop Table users");

        // create table
        try {
            stmt.executeUpdate("Create table users (id int primary key, name varchar(30))  ");
        }
        catch(SQLException sqlExc) {
            System.out.println("Table exists");
        }

        stmt.executeUpdate("DROP TABLE users");
        stmt.executeUpdate("Create table users (id int primary key, name varchar(30), address varchar(100))");

        // insert 2 rows
        try {
            stmt.executeUpdate("insert into users values (1,'tom')");
            stmt.executeUpdate("insert into users values (2,'peter', 'Milou 32')");
            stmt.executeUpdate("insert into users values (3, 'bob', 'Naxou 53')");
        } catch (SQLException e) {
            e.printStackTrace();
        }


        // query
        ResultSet rs = stmt.executeQuery("SELECT * FROM users");

        // print out query result
        while (rs.next()) {
            System.out.printf("%d\t%s\n", rs.getInt("id"), rs.getString("name"));
        }
    }
}