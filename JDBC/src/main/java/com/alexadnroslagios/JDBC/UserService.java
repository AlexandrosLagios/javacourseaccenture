package com.alexadnroslagios.JDBC;

import java.sql.*;

public class UserService {
    String dbUrl;
    Connection conn;

    public UserService() throws SQLException {
        this.dbUrl = "jdbc:sqlserver://localhost:1433;instance=SQLEXPRESS;databaseName=Test;";
        String user="alexandroslagios";
        String password="1234";
        this.conn = DriverManager.getConnection(dbUrl, user, password);
    }


    public void setUserName(String name) throws SQLException {
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("UPDATE users SET name = '" + name + "' WHERE ID = 1");
        ResultSet rs = stmt.executeQuery("SELECT * FROM users");
        while (rs.next()) {
            System.out.printf("%d\t%s\n", rs.getInt("id"), rs.getString("name"));
        }
    }

    public void addUser(String name) throws SQLException {
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO users (id, name) VALUES (3, '" + name + "')");
    }

    public void addColumn(String columnName) throws SQLException {
        Statement stmt = conn.createStatement();
        stmt.executeUpdate("ALTER TABLE users ADD " + columnName + " varchar(100)");
    }
}
