package com.alexandroslagios.springhelloworld.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;


@AllArgsConstructor
public class EmployeeError extends Exception {
    @Getter private String errorMessage;
}
