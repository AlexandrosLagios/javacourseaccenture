package com.alexandroslagios.springhelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication (scanBasePackages =
        {"com.alexandroslagios.springhelloworld.controllers"
                , "com.alexandroslagios.springhelloworld.services"
                , "com.alexandroslagios.springhelloworld.repositories"})
public class SpringhelloworldApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringhelloworldApplication.class, args);
  }
}
