package com.alexandroslagios.springhelloworld.controllers;

import com.alexandroslagios.springhelloworld.model.Employee;
import com.alexandroslagios.springhelloworld.services.EmployeeService;
import com.alexandroslagios.springhelloworld.util.EmployeeError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class RestApiController {
    // public static final Logger log = LoggerFactory.getLogger(RestApiController.class);
    
    @Autowired
    EmployeeService employeeService;
    
    @RequestMapping("/")
    public String sayHello() {
        return "Hello Spring!";
    }
    
    @RequestMapping(value = "/hi", method = RequestMethod.GET)
    public ResponseEntity<String> sayHi() {
        log.debug("Entering {}", "Saying hi");
        String hi =  "Hi!";
        log.debug("leaving {}", "Saying hi");
        return new ResponseEntity<String> (hi, HttpStatus.OK);
    }
    
    @GetMapping(value = "/employees")
    public ResponseEntity<List<Employee>> listAllEmployees() {
        List<Employee> employees = employeeService.findAllEmployees();
        if (employees.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        
        return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
    }
    
    @GetMapping(value = "/employee/{id}")
    public ResponseEntity<?> getEmployee(@PathVariable("id") long id) {
//        log.info("Fetching Employee with id {}", id);
        
        Employee employee = employeeService.findEmployeeById(id).orElse(null);
        if (employee == null) {
            log.error("No employee with id " + id + " exists!");
            return new ResponseEntity(new EmployeeError("Employee with id" + id
            + id + "not found"), HttpStatus.NOT_FOUND);
        }
        else {
            log.info("Employee with " + id + " was fetched.");
            return new ResponseEntity<Employee>(employee, HttpStatus.OK);
        }
        
    }
    
    @DeleteMapping(value = "/employee/{id}")
    public ResponseEntity<?> deleteEmployee(@PathVariable("id") long id) {
        log.info("Fetching & Deleting Employee with id {}", id);
        Employee employee = employeeService.findEmployeeById(id).get();
        
        if (employee == null) {
            log.error("Unable to delete. Employee with id {} not found.", id);
            return new ResponseEntity(
                    new EmployeeError("Unable to delete. Employee with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        
        employeeService.deleteEmployeeById(id);
        return new ResponseEntity<Employee>(HttpStatus.NO_CONTENT);
    }
    
//    @PostMapping
//    public
}
