package com.alexandroslagios.springhelloworld.repositories;

import com.alexandroslagios.springhelloworld.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
