package com.alexandroslagios.springhelloworld.services;

import com.alexandroslagios.springhelloworld.model.Employee;
import com.alexandroslagios.springhelloworld.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;
    
    @Override
    public List<Employee> findAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        add(new Employee("Manolis"));
        add(new Employee("Vaggelis"));
        add(new Employee("Bob"));
        add(new Employee("Takis"));
        employeeRepository.findAll().forEach(employee -> employees.add(employee));
        return employees;
    }
    
    @Override
    public Optional<Employee> findEmployeeById(long id) {
        return employeeRepository.findById(id);
    }
    
    @Override
    public boolean deleteEmployeeById(long id) {
        employeeRepository.deleteById(id);
        return true ? employeeRepository.findById(id) != null : false;
    }
    
    public void add(Employee employee) {
        employeeRepository.save(employee);
    }
}
