package com.alexandroslagios.springhelloworld.services;

import com.alexandroslagios.springhelloworld.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface EmployeeService {
    public List<Employee> findAllEmployees();
    public Optional<Employee> findEmployeeById(long id);
    public boolean deleteEmployeeById(long id);
}
