import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static boolean isPrime(int n){
        for(int i=3; i < n/2; n++) {
            if(n % i == 0) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> randomNumbers = random.ints(1000)
                .sorted().boxed().collect(Collectors.toList());
        List<Integer> oddNumbers = randomNumbers.stream().filter(i -> i % 2 != 0).collect(Collectors.toList());
        oddNumbers.stream().forEach(System.out::println);
        int min = randomNumbers.get(0);
        int max = randomNumbers.get(randomNumbers.size()-1);
        int count = (int) randomNumbers.stream().count();
        IntSummaryStatistics stats = randomNumbers.stream().mapToInt(i -> i).summaryStatistics();
        double avg = randomNumbers.stream().mapToInt(i -> i).average().getAsDouble();
        int oddMin = oddNumbers.stream().mapToInt(i -> i).min().getAsInt();
        int oddMax = oddNumbers.stream().mapToInt(i -> i).max().getAsInt();
        int oddCount = (int) oddNumbers.stream().count();
        double oddAvg = oddNumbers.stream().mapToInt(i -> i).average().getAsDouble();
        double evenAvg = randomNumbers.stream().filter(i -> i % 2 == 0).mapToInt(i -> i).average().getAsDouble();
        int evenCount = (int) randomNumbers.stream().filter(i -> i % 2 == 0).mapToInt(i -> i).count();
        List<Integer> primeNumbers = oddNumbers.stream().filter(i -> isPrime(i)).collect(Collectors.toList());
        System.out.println(stats);
        System.out.println("The average of the random number list is " + avg);
        System.out.println("The count of the random number list is " + count);
        System.out.println("The minimum random number is " + min);
        System.out.println("The maximum random number is " + max);
        System.out.println("There are " + oddCount + " odd numbers in the random number list");
        System.out.println("The minimum odd number is " + oddMin);
        System.out.println("The maximum odd number is " + oddMax);
        System.out.println("The average of the odd numbers is " + oddAvg);
        System.out.println("There are " + (randomNumbers.size() - oddCount) + " even numbers in the random number list");
        System.out.println("There are " + evenCount + " even numbers in the random number list");
        System.out.println("The average of the even numbers is " + evenAvg);
        System.out.println("There are " + primeNumbers.size() + " prime numbers in the random number list");
    }
}
